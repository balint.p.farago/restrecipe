package com.matritel.recipe.controller;

import com.matritel.recipe.model.RecipeDto;
import com.matritel.recipe.service.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "api/matritel/recipebook/recipes")
public class RecipeController {

    @Autowired
    private RecipeService recipeService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public RecipeDto post (@Valid @RequestBody RecipeDto inputRecipe){
        return recipeService.createRecipe(inputRecipe);
    }

    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public RecipeDto getById(@PathVariable String id){
        if (recipeService.getRecipeById(id).isPresent()) {
            return recipeService.getRecipeById(id).get();
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Recipe is not found by the given id!");
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<RecipeDto> getAll(){
        return recipeService.getAllRecipe();
    }

    @GetMapping(value = "/findByCreator")
    @ResponseStatus(HttpStatus.OK)
    public List<RecipeDto> getAllByCreator(@RequestParam String creatorName){
        return recipeService.getAllRecipeByCreator(creatorName);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable String id){
        recipeService.deleteRecipeById(id);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse mapMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException ex) {
        return new ErrorResponse("Wrong request: " + ex.getMessage());
    }

    /*@ExceptionHandler(InvalidFormatException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse mapInvalidFormatException(InvalidFormatException ex){
        return new ErrorResponse("Value is not one of possible measurement types: [GRAMS, MILLILITERS]");
    }*/

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse mapMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        return new ErrorResponse("wrong argument: " + ex.getMessage());
    }

    // I have to throw ResponseStatusException when resource was not found, I have to handle that here instead of EntityNotFoundException
    @ExceptionHandler(ResponseStatusException.class)
    // If I use ResponseStatusException I don't have to provide explicitly the ResponseStatus it will be provided by the exception
    //@ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorResponse mapMethodResponseStatusException(ResponseStatusException ex) {
        return new ErrorResponse("Response problem: " + ex.getMessage());
    }

    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse mapRuntimeException(RuntimeException ex) {
        return new ErrorResponse("Internal Server Error");
    }
}
