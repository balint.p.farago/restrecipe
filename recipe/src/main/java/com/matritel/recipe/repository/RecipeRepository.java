package com.matritel.recipe.repository;

import com.matritel.recipe.repository.entity.RecipeEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RecipeRepository extends CrudRepository<RecipeEntity, String> {

    List<RecipeEntity> findAll();

    List<RecipeEntity> findAllByCreatorName(String creatorName);
}
