package com.matritel.recipe.repository.entity;

import com.matritel.recipe.model.IngredientDto;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity(name = "recipe_book") // we can give the table name as a parameter
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RecipeEntity {

    @Id
    @GeneratedValue(generator = "uuid2") // this is a specific id generator in Hibernate
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;

    @Column(columnDefinition = "VARCHAR(80)")
    private String title;

    @Column(columnDefinition = "VARCHAR(255)")
    private String instructions;

    @ElementCollection
    @Embedded
    private List<IngredientDto> ingredients;

    @Column(name= "creator_name", columnDefinition = "VARCHAR(20)")
    private String creatorName;

    @Column
    private LocalDate date;
}
