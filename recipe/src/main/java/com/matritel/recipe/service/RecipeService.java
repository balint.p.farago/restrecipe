package com.matritel.recipe.service;

import com.matritel.recipe.model.RecipeDto;
import com.matritel.recipe.repository.RecipeRepository;
import com.matritel.recipe.repository.entity.RecipeEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class RecipeService {

    @Autowired
    private RecipeRepository repository;

    public RecipeDto createRecipe(RecipeDto inputRecipe) {
        RecipeEntity toSave = toEntity(inputRecipe);
        RecipeEntity saved = repository.save(toSave);
        return toDto(saved);
    }

    public List<RecipeDto> getAllRecipe() {
        List<RecipeEntity> listOfEntities = repository.findAll();
        List<RecipeDto> listOfDtos = new ArrayList<>();

        for (RecipeEntity anEntity: listOfEntities) {
            listOfDtos.add(toDto(anEntity));
        }
        return listOfDtos;
    }

    public List<RecipeDto> getAllRecipeByCreator(String creatorName) {
        List<RecipeEntity> listOfEntities = repository.findAllByCreatorName(creatorName);
        List<RecipeDto> listOfDtos = new ArrayList<>();

        for (RecipeEntity anEntity : listOfEntities) {
            listOfDtos.add(toDto(anEntity));
        }
        return listOfDtos;
    }

    public Optional<RecipeDto> getRecipeById(String id) {
        Optional<RecipeEntity> recipeEntity = repository.findById(id);
/*
        if (recipeEntity.isPresent()){
            return Optional.of(toDto(recipeEntity.get()));
        }
        return Optional.empty();
*/
        return recipeEntity.map(this::toDto);

    }

    private RecipeDto toDto(RecipeEntity entity){
        return RecipeDto.builder()
                .id(entity.getId())
                .title(entity.getTitle())
                .instructions(entity.getInstructions())
                .ingredients(entity.getIngredients())
                .creatorName(entity.getCreatorName())
                .date(entity.getDate())
                .build();
    }

    private RecipeEntity toEntity(RecipeDto dto){
        return RecipeEntity.builder()
                .title(dto.getTitle())
                .instructions(dto.getInstructions())
                .ingredients(dto.getIngredients())
                .creatorName(dto.getCreatorName())
                .date(LocalDate.now())
                .build();
    }

    public void deleteRecipeById(String id) {
        repository.deleteById(id);
    }
}
