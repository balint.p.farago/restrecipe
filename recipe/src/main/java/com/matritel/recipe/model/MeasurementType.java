package com.matritel.recipe.model;

public enum MeasurementType {

    GRAMS, MILLILITERS
}
