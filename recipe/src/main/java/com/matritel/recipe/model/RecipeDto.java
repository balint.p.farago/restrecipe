package com.matritel.recipe.model;

import lombok.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RecipeDto {

    private String id;

    @NotNull
    @Size(max = 80)
    private String title;

    @NotNull
    @Size(max = 255)
    private String instructions;

    @NotNull
    @Size(max = 255)
    private List<IngredientDto> ingredients;

    @NotNull
    @Size(max = 20)
    private String creatorName;

    private LocalDate date;
}
