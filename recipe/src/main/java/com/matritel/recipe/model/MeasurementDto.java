package com.matritel.recipe.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MeasurementDto {

    @Size(max = 10)
    @NotNull
    private int amount;

    @Enumerated(EnumType.STRING)
    @NotNull
    private MeasurementType type;
}
